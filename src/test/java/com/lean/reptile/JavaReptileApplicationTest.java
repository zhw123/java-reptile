package com.lean.reptile;

import com.lean.reptile.spider.sp.ZonghengTask;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JavaReptileApplicationTest {

    @Autowired
    ZonghengTask zonghengTask;

    @Test
    public void test() {
        zonghengTask.doTask();
    }

}
