-- spider.novel definition

CREATE TABLE `novel` (
                                `id` int(32) NOT NULL AUTO_INCREMENT,
                                `name` varchar(100) DEFAULT NULL,
                                `url` varchar(100) DEFAULT NULL,
                                `author` varchar(100) DEFAULT NULL,
                                `desc` mediumtext,
                                `chapter` varchar(255) DEFAULT NULL,
                                `imgUrl` varchar(255) DEFAULT NULL,
                                `word_number` varchar(100) DEFAULT NULL,
                                `toatl_recommend_number` varchar(100) DEFAULT NULL,
                                `total_click_count` varchar(100) DEFAULT NULL,
                                `week_recommend_number` varchar(100) DEFAULT NULL,
                                `status` varchar(100) DEFAULT NULL,
                                `last_update_time` varchar(100) DEFAULT NULL,
                                `tags` varchar(100) DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
