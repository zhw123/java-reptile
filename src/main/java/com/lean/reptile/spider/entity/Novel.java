package com.lean.reptile.spider.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Novel implements Serializable {
    private Long id;
    private String name;
    private String url;
    private String author;
    private String tags;
    private String status;
    private String desc;
    private String chapter;
    private String imgUrl;
    private String wordNumber;
    private String toatlRecommendNumber;
    private String totalClickCount;
    private String weekRecommendNumber;
    private String lastUpdateTime;
}
