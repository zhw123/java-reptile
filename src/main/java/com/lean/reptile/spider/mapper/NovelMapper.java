package com.lean.reptile.spider.mapper;

import com.lean.reptile.spider.entity.Novel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NovelMapper {
    void batchInsert(List<Novel> novels);

    void insertNovel(Novel novels);

    void updateNovel(Novel novel);

    Novel findNovelById(Long id);
}
