package com.lean.reptile.webmagic;

import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.scheduler.component.DuplicateRemover;

/**
 * @ClassName: MyScheduler
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/5/9 16:05
 */
public class MyScheduler implements DuplicateRemover {
    @Override
    public boolean isDuplicate(Request request, Task task) {
        return false;
    }

    @Override
    public void resetDuplicateCheck(Task task) {

    }

    @Override
    public int getTotalRequestsCount(Task task) {
        return 0;
    }

    /**
     *在解析页面的时候，很可能会解析出相同的url地址(例如商品标题和商品图片超链接，而且url一样)，如果不进行处理，同样的url会解析处理多次，浪费资源。所以我们需要有一个url去重的功能。
     * URL保存
     * WebMagic提供了Scheduler可以帮助我们解决以上问题。
     *
     * Scheduler是WebMagic中进行URL管理的组件。一般来说，Scheduler包括两个作用：
     *
     * 1）对待抓取的URL队列进行管理。
     *
     * 2）对已抓取的URL进行去重。
     *
     * WebMagic内置了几个常用的Scheduler。如果你只是在本地执行规模比较小的爬虫，那么基本无需定制Scheduler，但是了解一下已经提供的几个Scheduler还是有意义的。
     *
     *类	说明	备注
     * DuplicateRemovedScheduler	抽象基类，提供一些模板方法	继承它可以实现自己的功能
     * QueueScheduler	使用内存队列保存待抓取URL
     * PriorityScheduler	使用带有优先级的内存队列保存待抓取URL	耗费内存较QueueScheduler更大，但是当设置了request.priority之后，只能使用PriorityScheduler才可使优先级生效
     * FileCacheQueueScheduler	使用文件保存抓取URL，可以在关闭程序并下次启动时，从之前抓取到的URL继续抓取	需指定路径，会建立.urls.txt和.cursor.txt两个文件
     * RedisScheduler	使用Redis保存抓取队列，可进行多台机器同时合作抓取	需要安装并启动redis
     *
     *URL去重
     * 去重部分被单独抽象成了一个接口：DuplicateRemover，从而可以为同一个Scheduler选择不同的去重方式，以适应不同的需要，目前提供了两种去重方式。
     *
     *类	说明
     * HashSetDuplicateRemover	使用HashSet来进行去重，占用内存较大
     * BloomFilterDuplicateRemover	使用BloomFilter来进行去重，占用内存较小，但是可能漏抓页面
     * 所有默认的Scheduler都使用HashSetDuplicateRemover来进行去重，（除开RedisScheduler是使用Redis的set进行去重）。如果你的URL较多，使用HashSetDuplicateRemover会比较占用内存，所以也可以尝试以下BloomFilterDuplicateRemover1，使用方式：
     *如果使用BloomFilterDuplicateRemover，需要单独引入Guava依赖包。
     *
     *RedisScheduler是使用Redis的set进行去重，其他的Scheduler默认都使用HashSetDuplicateRemover来进行去重。
     *
     *<!--WebMagic对布隆过滤器的支持-->
     * <dependency>
     *     <groupId>com.google.guava</groupId>
     *     <artifactId>guava</artifactId>
     *     <version>16.0</version>
     * </dependency>
     *
     * 三种去重方式
     * 去重就有三种实现方式，那有什么不同呢？
     *
     * HashSet
     *
     * 使用java中的HashSet不能重复的特点去重。优点是容易理解。使用方便。
     *
     * 缺点：占用内存大，性能较低。
     *
     * Redis去重
     *
     * 使用Redis的set进行去重。优点是速度快（Redis本身速度就很快），而且去重不会占用爬虫服务器的资源，可以处理更大数据量的数据爬取。
     *
     * 缺点：需要准备Redis服务器，增加开发和使用成本。
     *
     * 布隆过滤器（BloomFilter）
     *
     * 使用布隆过滤器也可以实现去重。优点是占用的内存要比使用HashSet要小的多（大概只占HashSet内存的八分之一），也适合大量数据的去重操作。
     *
     * 缺点：有误判的可能。没有重复可能会判定重复，但是重复数据一定会判定重复。
     *
     * 性价比最高，推荐使用！
     *
     */


}
