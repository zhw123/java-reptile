package com.lean.reptile.webmagic;


import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.scheduler.BloomFilterDuplicateRemover;
import us.codecraft.webmagic.scheduler.QueueScheduler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName: JobProcessor
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/5/9 17:14
 */
public class JobProcessor implements PageProcessor {

    /**
     * 方法说明示例:
     setCharset(String)
     设置编码，示例：site.setCharset("utf-8")
     setUserAgent(String)
     设置UserAgent，示例：site.setUserAgent("Spider")
     setTimeOut(int)
     设置超时时间，单位是毫秒，示例：site.setTimeOut(3000)
     setRetryTimes(int)
     设置重试次数，示例：site.setRetryTimes(3)
     setCycleRetryTimes(int)
     设置循环重试次数，示例：site.setCycleRetryTimes(3)
     addCookie(String,String)
     添加一条cookie，示例：site.addCookie("dotcomt_user","code4craft")
     setDomain(String)
     设置域名，需设置域名后，addCookie才可生效，示例：site.setDomain("github.com")
     addHeader(String,String)
     添加一条addHeader，示例：site.addHeader("Referer","https://github.com")
     setHttpProxy(HttpHost)
     设置Http代理，示例：site.setHttpProxy(new HttpHost("127.0.0.1",8080))
     */
    private Site site = Site.me().setDomain("my.oschina.net").setRetryTimes(3).setSleepTime(1000).setTimeOut(3000);

    /**
     * 主方法启动爬虫
     */
    public static void main(String[] args) {

        Spider.create(new JobProcessor())
                // 添加初始化的URL
                .addUrl("http://my.oschina.net/flashsword/blog")
                // 使用单线程
                .thread(1)
                //使用Pipeline保存结果
                .addPipeline(new MyPipeline())
                //.addPipeline(new FilePipeline("C:\\Users\\tree\\Desktop\\result"))
                //设置布隆去重过滤器，指定最多对1000万数据进行去重操作
                .setScheduler(new QueueScheduler().setDuplicateRemover(new BloomFilterDuplicateRemover(10000000)))
                // 运行
                .run();
    }

    boolean firstFlag=false;
    /**
     * 页面处理逻辑
     * 也就是访问主程序中的URL后得到的页面
     *
     * 爬虫思路：
     *      1. 主程序访问URL后得到页面
     *      2. 将得到的页面解析出需要的参数，并将解析出来并且需要爬取的链接放入爬取目标中 （45-59行）
     *          TIPS：WebMagic会自动去识别哪些连接爬取过哪些没有。
     *      3. 访问第二步中放入的链接得到页面，并解析（62行）
     *      4. 将图片的名字和后缀提取出来以便保存（64-77行）
     * 解析页面
     * gethtml()...all()方法如果不加，只爬取匹配到的第一个元素。
     * 方法              说明	             示例
     * xpath(String xpath)	使用XPath选择	html.xpath("//div[@class='title']")
     * $(String selector)	使用Css选择器选择	html.$("div.title")
     * $(String selector,String attr)	使用Css选择器选择	html.$("div.title","text")
     * css(String selector)	功能同$()，使用Css选择器选择	html.css("div.title")
     * links()	选择所有链接	html.links()
     * regex(String regex)	使用正则表达式抽取	html.regex("\(.\*?)\")
     * regex(String regex,int group)	使用正则表达式抽取，并指定捕获组	html.regex("\(.\*?)\",1)
     * replace(String regex, String replacement)	替换内容	html.replace("\","")
     */
    public void process(Page page) {
        //进入页面详情
       List<String> links = page.getHtml().links().regex("https://my.oschina.net/flashsword/blog/\\d+").all();
       page.addTargetRequests(links);
       if(!firstFlag){
           firstFlag=true;
           page.putField("titles", page.getHtml().xpath("//*[@id=\"newestBlogList\"]/div/div/div/a/text()").all());
           page.putField("desc", page.getHtml().xpath("//*[@id=\"newestBlogList\"]/div/div/div/div/p/text()").all());
           page.putField("tags",page.getHtml().xpath("//*[@id=\"newestBlogList\"]/div/div/div/div/div/div/a/text()").all());
       }else {
           page.putField("title", page.getHtml().xpath("//*/div[@class='article-box__inner']/div[@class='article-box__header']/h1/a/text()").toString());
           page.putField("content", page.getHtml().$("div.content").toString());
           page.putField("tags",page.getHtml().css("a.link--default").toString());
           page.putField("tags1",page.getHtml().xpath("//*/a[@class='link--default']/text()").toString());

       }
//        不继续爬取
//        page.setSkip(true);
    }


    public Site getSite() {
        return site;
    }
}
