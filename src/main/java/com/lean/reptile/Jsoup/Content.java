package com.lean.reptile.Jsoup;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @ClassName: Content
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/5/7 16:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Content {

    private String img;
    private String title;
    private String price;
}
