package com.lean.reptile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 爬虫：
 *     https://bright-boy.gitee.io/technical-notes/#/java/java%E7%88%AC%E8%99%AB?id=_1%e7%88%ac%e8%99%ab%e5%9f%ba%e7%a1%80%e6%a6%82%e5%bf%b5
 */
@SpringBootApplication
public class JavaReptileApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaReptileApplication.class, args);
    }

}
